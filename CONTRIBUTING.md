Thank you for your interest in contributing to this project.  
 
Why your contribution is meaningful? 
 
This project is still in its early stages. While the initial version of the PCB was manufactured to best of the initial contributor’s ability, there were still some features that didn’t work as expected. Your contribution will help alleviate these mistakes and make the PCB better through the addition of new features.  
 
How to contribute  
 
1.	Getting Access to the repository 
 
If you would like to contribute, kindly send an email to msmluy006@myuct.ac.za with a short paragraph motivating what feature you would like to add/fix/improve and why. After your motivation has been reviewed and approved you will receive and email detailing your approval. From there, you will have access to the project’s repository.  
 
1.	Finding something broken in the project  
 
If you find a broken feature, please report it on Gitlab. Furthermore, if you have a solution to the problem by all means, make a pull request with you solution. Once your pull request has been approved, you will be allowed to implement your solution.  
 
1.	Tools to use 
 
The code for the project was written and compiled in STMCubeIDE. It is, therefore, advisable that you also use STMCubeIDE unless you are well versed in other IDE's. There are no specific programming standards and best practices that are unique to this project. However, do ensure you follow the general coding best practises such as commenting your code, using readable variable names etc.  
 
 
A final note 
 
The PCB was designed as a fun way to aid plant growth. However, the projects capabilities are not only limited to monitoring plants. If you have innovative ideas of how to extend the projects capabilities by all means, make your ideas known. Finally, have fun whilst you are at it 😉  
 “
