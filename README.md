hardware required.
•	STM32 Discovery board.
•	Micro USB Type B cable.
software required.
•	STM32CubeIDE/ Atollic True Studio
•	ST-Link/J-Link GDB Server.
•	CP2101 USB-UART bridge drivers

how to connect
•	Simple plug the HAT on top of Discovery Board via the Female connectors provided with the board.
•	Connect the USB cable to connect to a computer.
hello-world
•	The Hello-Word program can be downloaded from the git repo.
•	Simply pressing “Run” on STM32CubeIDE should upload the code to Discovery Board
